char *ft_strrev(char *str)
{
	char *strrev;
	int i;
	int j;

	i = 0;
	j = 0;

	while (str[i] != '\0')
	{
		i++;
	}
	
	i--;	
	
	while (i >= 0)
	{
		strrev[j] = str[i];
		i--;
		j++;
	}

	strrev[j] = '\0';
	str = strrev;

	return str;
}
