#include "../../ft_putchar/ft_putchar.h"

void ft_putstr(char *str) 
{
	int i;

	i = 0;

	while (str[i] != '\0') 
	{
		ft_putchar(*str);
		i++;
	}
	ft_putchar('\n');
}
